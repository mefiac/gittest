<?php

class Configuration
{
    static private $params = [
        'database' => ['connection' => 'mysql:',
            'host' => 'localhost',
            'dbname' => 'api',
            'user' => 'root',
            'password' => ''

        ],
        'github' => [
            'login' => 'asd',
            'pass' => 'etc'
        ]
    ];

    /**
     * @param $param - that param you need/ example database
     * @return mixed
     */
    public static function get($param)
    {
        return self::$params[$param];
    }
}

?>