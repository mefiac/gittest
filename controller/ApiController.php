<?php

class ApiController extends BaseController
{

    public function actionUser()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.github.com/users");
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $answer = json_decode(curl_exec($ch), true);

        if (!empty($answer)) {

            $model = new user();
            $model->insert_user('user', $answer);
        }
        curl_close($ch);

    }
}