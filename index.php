<?php

define("ROOT_DIR",dirname(__FILE__).'/');

require_once "vendor/autoload.php";
require_once "vendor/main.php";

$application = new Application();	
$application->run();

