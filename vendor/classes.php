<?php
return [
	'Configuration' =>  'config/Configuration.php',
	'BaseController' => 'vendor/BaseController.php',
    'DB' =>  'vendor/database.php',
    'BaseModel' =>'vendor/BaseModel.php',
];