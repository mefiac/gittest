<?php

class Application
{

    public function run()
    {
        $this->Loader();
    }

    public function Route()
    {

        $argv = $_SERVER['argv'];
        $controller = $argv[1] . 'Controller';
        $controller = new $controller();
        $controller->url = (!empty($url[1])) ? $url[1] : null;
        $controller->method = $_SERVER['REQUEST_METHOD'];

        if (!empty($argv[2]) && !is_int($argv[2])) {

            call_user_func(array($controller, 'action' . strtolower($argv[2])));
        }

    }

    public function Loader()
    {
        spl_autoload_register(['ClassLoader', 'autoload'], true, true);

        try {
            $this->Route();

        } catch (Exception $e) {
            echo '<h2>Внимание! Обнаружена ошибка.</h2>' .
                '<h4>' . $e->getMessage() . '</h4>' .
                '<pre>' . $e->getTraceAsString() . '</pre>';
            exit;
        }
    }


}
