<?php

class BaseModel
{
    public function __construct()
    {
        $params= Configuration::get('database');
        $this->database = new DB($params);
    }
}