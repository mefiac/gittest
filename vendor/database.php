<?php

class DB
{
    public $a;
    protected $db;

    function __construct($params, PDO $db = null)
    {
        $this->db = $db;

        try {
            if ($this->db === null) {
                $this->db = new PDO('mysql:host=' . $params['host'] . ';dbname=' . $params['dbname'],
                    $params['user'], $params['password']);
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function stringBuilder($table, $values)
    {

        $sql = 'INSERT INTO ' . $table . ' (`github_id`, `github_login`) VALUES ';
        $temp = [];

        foreach ($values as $val) {
            $temp[] = "(" . $val['id'] . ", '" . $val['login'] . " ')";

        }
        $sql .= join(', ', $temp) . ' ON DUPLICATE KEY UPDATE github_login=VALUES(github_login) ;';
        return $sql;
    }

    public function queryAll($query)
    {
        try {
            $query = $this->db->prepare($query);
            $query->execute();
            return $query->fetchAll();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function queryOne($query)
    {
        try {
            $query = $this->db->prepare($query);
            $query->execute();
            return $query->fetch();
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function query($query)
    {
        try {
            $query = $this->db->prepare($query);
            $query->execute();
            return $this->db->lastInsertId();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    function __destruct()
    {
        $this->db = null;
    }
}

?>